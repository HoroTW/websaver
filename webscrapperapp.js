// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')

const fs = require('fs-extra');

puppeteer.use(StealthPlugin())

url = 'https://medium.com/@dominikangerer/headless-cms-explained-in-5-minutes-450f063850f7'



domain_name = url.match(/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]/)
folder_name = 'output/'+domain_name+'/'
m_timeout = 60000

fs.mkdirp(__dirname + '/' + folder_name)

// puppeteer usage as normal

puppeteer.launch({ headless: true, defaultViewport: {isMobile: false, width:1920, height:1080} }).then(async browser => {
  console.log('Running normal..')
  const page = await browser.newPage()
  await page.goto(url,{waitUntil: 'networkidle2', timeout: m_timeout})
  await page.waitFor(5000)

  await autoScroll(page);
  await autoScroll(page);

  await page.screenshot({ path: folder_name+'normalScreenshot.png', fullPage: true })


  const cdp = await page.target().createCDPSession();
  const { data } = await cdp.send('Page.captureSnapshot', { format: 'mhtml' });
  fs.writeFileSync(folder_name+'normalSnapshot.mhtml', data);

  await browser.close()
  console.log(`All normal done ✨`)
})

puppeteer.launch({ headless: true, defaultViewport: {isMobile: false, width:1920, height:15800} }).then(async browser => {
  console.log('Running fallback..')
  const page = await browser.newPage()
  await page.goto(url,{waitUntil: 'networkidle2', timeout: m_timeout})
  await page.waitFor(5000)

  await page.screenshot({ path: folder_name+'fallback_long_screenshot.png', fullPage: true })

  const cdp = await page.target().createCDPSession();
  const { data } = await cdp.send('Page.captureSnapshot', { format: 'mhtml' });
  fs.writeFileSync(folder_name+'fallback_long_Snapshot.mhtml', data);

  await browser.close()
  console.log(`All fallback Done ✨`)
})

puppeteer.launch({ headless: true, defaultViewport: {isMobile: true, width:1080, height:19200} }).then(async browser => {
  console.log('Running mobile..')
  const page = await browser.newPage()
  await page.goto(url,{waitUntil: 'networkidle2', timeout: m_timeout})
  await page.waitFor(5000)
  await autoScroll(page);
  await autoScroll(page);

  await page.screenshot({ path: folder_name+'mobile_screenshot.png', fullPage: true })

  const cdp = await page.target().createCDPSession();
  const { data } = await cdp.send('Page.captureSnapshot', { format: 'mhtml' });
  fs.writeFileSync(folder_name+'mobileSnapshot.mhtml', data);


  await browser.close()
  console.log(`All mobile Done ✨`)
})

console.log('\nAll done!')

async function autoScroll(page){
  await page.evaluate(async () => {
      await new Promise((resolve, reject) => {
          var totalHeight = 0;
          var distance = 400;
          var timer = setInterval(() => {
              var scrollHeight = document.body.scrollHeight;
              window.scrollBy(0, distance);
              totalHeight += distance;

              if(totalHeight >= scrollHeight){
                  clearInterval(timer);
                  resolve();
              }
          }, 100);
      });
  });
}